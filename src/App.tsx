import { useState } from "react";
// import mainImage from './assets/images/main.jpg';
import List from "./componets/List";
import Add from "./componets/add";

export interface IState {
  IPersons: {
    id: number;
    fullName: string;
    age: number;
    imageUrl: string;
    bio?: string;
  }[];
  IPerson: {
    id: number;
    fullName: string;
    age: number;
    imageUrl: string;
    bio?: string;
  };
}
const App = () => {
  const [persons, setPersons] = useState<IState["IPersons"]>([
    {
      id: 1,
      fullName: "میلاد",
      age: 35,
      //   imageUrl: mainImage,
      imageUrl: "/assets/images/main.jpg", //or this way(import from public)

      bio: "",
    },
    {
      id: 2,
      fullName: "علی",
      age: 20,
      imageUrl: "/assets/images/main.jpg", //or this way(import from public)
      bio: "web developer",
    },
  ]);

  return (
    <div className="container">
      <div className="alert alert-info">مدیریت اشخاص</div>
      <List persons={persons} setPersons={setPersons} />

      <Add persons={persons} setPersons={setPersons} />
    </div>
  );
};

export default App;
