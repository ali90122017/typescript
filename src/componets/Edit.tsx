import { Dispatch, FC, FormEvent, SetStateAction, useState } from "react";
import { AiFillEdit } from "react-icons/ai";
import Modal from "react-bootstrap/Modal";
import { IState } from "../App";

interface IProps {
  person: IState["IPerson"];
  persons: IState["IPersons"];
  setPersons: Dispatch<SetStateAction<IState["IPersons"]>>
}

const Edit: FC<IProps> = ({ person, persons, setPersons }) => {
  const [showModal, setShowModal] = useState<boolean>(false);
  const [fullName, setFullName] = useState<string>(person.fullName);
  const [age, setAge] = useState<string | number>(person.age);
  const [imageUrl, setImageUrl] = useState<string>(person.imageUrl);
  const [bio, setBio] = useState<string | undefined>(person.bio);

  const handleSubmitForm = (e: FormEvent<HTMLFormElement>) :void => {
    e.preventDefault();

    const copyPersons = [...persons];
    const index = copyPersons.findIndex(p => p.id = person.id);
    copyPersons[index] = {
        id:person.id,
        fullName,
        age: Number(age), // type must be number ***
        imageUrl,
        bio
    }

    setPersons(copyPersons);

    setShowModal(false);

  }

  return (
    <>
      <AiFillEdit
        className="text-primary m-1"
        onClick={() => setShowModal(true)}
        size={30}
      />

      {/* Edit Modal */}
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            ویرایش کاربر
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <form
              autoComplete="off"
              className="mt-3"
              onSubmit={(e) => handleSubmitForm(e)}
            >
              <input
                type="text"
                className="form-control mb-2"
                name="fullName"
                value={fullName}
                onChange={(e) => setFullName(e.target.value)}
                placeholder="نام و نام خانوادگی"
              />
              <input
                type="number"
                className="form-control mb-2"
                name="age"
                value={age}
                onChange={(e) => setAge(e.target.value)}
                placeholder="سن"
              />
              <input
                type="text"
                className="form-control mb-2"
                name="imageUrl"
                value={imageUrl}
                onChange={(e) => setImageUrl(e.target.value)}
                placeholder="آدرس تصویر پروفایل"
              />
              <textarea
                className="form-control mb-2"
                name="bio"
                rows={7}
                value={bio}
                onChange={(e) => setBio(e.target.value)}
                placeholder="بیوگرافی"
              />
              <button type="submit" className="btn btn-success ms-2" >
                افزودن به لیست
              </button>
              <button type="button" onClick={() => setShowModal(false)} className="btn btn-danger">
                بستن
              </button>
            </form>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Edit;
