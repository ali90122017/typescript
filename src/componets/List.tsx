import { Dispatch, FC, SetStateAction } from "react";
import { IState } from "../App";
import { AiOutlineUserDelete } from "react-icons/ai";
import Edit from "./Edit";

interface IProps {
  persons: IState["IPersons"];
  setPersons: Dispatch<SetStateAction<IState["IPersons"]>>;
}

// const List = ({ persons }: IProps) => {
const List: FC<IProps> = ({ persons, setPersons }) => {
  const handleDeletePerson = (id: number): void => {
    console.log(id);
    const p: IState["IPersons"] = [...persons];
    const filteredPersons: IState["IPersons"] = p.filter(
      (item) => item.id != id
    );
    setPersons(filteredPersons);
  };

  return (
    <div className="row">
      {persons.map((p) => {
        return (
          <div key={p.id} className="col-12 col-lg-6">
            <div className="card ">
              <div className="card-body d-flex align-items-center">
                <img
                  className=" img-thumbnail rounded"
                  width={200}
                  height={200}
                  src={p.imageUrl}
                  alt={p.fullName}
                />
                <div className="me-3">
                  <span>{p.fullName}</span>
                  <span className="bg bg-primary rounded-pill me-3 ms-3 p-1 text-white">
                    {" "}
                    {`سال ${p.age} `}
                  </span>
                  <p className="text-muted">{p.bio}</p>
                </div>
              </div>
              <div className="operation_btns">
                <AiOutlineUserDelete
                  onClick={() => handleDeletePerson(p.id)}
                  className="text-danger m-1 "
                  size={30}
                />
                <Edit person = {p} persons = {persons} setPersons = {setPersons} />
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default List;
