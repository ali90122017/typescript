import { Dispatch, FC, FormEvent, SetStateAction, useState } from "react";
import { IState } from "../App";

interface IProps {
    persons : IState["IPersons"];
    setPersons: Dispatch<SetStateAction<IState["IPersons"]>>
}

const Add : FC<IProps> = ({persons, setPersons}) => {
  const [fullName, setFullName] = useState<string>("");
  const [age, setAge] = useState<string | number>("");
  const [imageUrl, setImageUrl] = useState<string>("");
  const [bio, setBio] = useState<string>("");

  const handleResetState = (): void => {
    setFullName("");
    setAge("");
    setImageUrl("");
    setBio("");
  };

  const handleSubmitForm = (e:FormEvent<HTMLFormElement>) : void => {
    e.preventDefault();

    // if(!fullName) {
    //     return alert("نام الزامی می باشد."); 
    // }

    const user = {
        id:Math.floor(Math.random() *1000),
        fullName,
        age: Number(age), // type must be number ***
        imageUrl,
        bio
    }

    
    const p = [...persons];
    p.push(user);

    setPersons(p);
    console.log(persons)
    console.log(p)

    // setPersons([...persons, user])

    handleResetState();
  }

  console.log(persons);
  

  return (
    <div className="col-md-6 col-lg-6 mx-auto">
      <form autoComplete="off" className="mt-3" onSubmit={(e) => handleSubmitForm(e)}>
        <input
          type="text"
          className="form-control mb-2"
          name="fullName"
          value={fullName}
          onChange={(e) => setFullName(e.target.value)}
          placeholder="نام و نام خانوادگی"
        />
        <input
          type="number"
          className="form-control mb-2"
          name="age"
          value={age}
          onChange={(e) => setAge(e.target.value)}
          placeholder="سن"
        />
        <input
          type="text"
          className="form-control mb-2"
          name="imageUrl"
          value={imageUrl}
          onChange={(e) => setImageUrl(e.target.value)}
          placeholder="آدرس تصویر پروفایل"
        />
        <textarea
          className="form-control mb-2"
          name="bio"
          rows={7}
          value={bio}
          onChange={(e) => setBio(e.target.value)}
          placeholder="بیوگرافی"
        />
        <button type="submit" className="btn btn-success">
          افزودن به لیست
        </button>
      </form>
    </div>
  );
};

export default Add;
